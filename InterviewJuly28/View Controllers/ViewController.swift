//
//  ViewController.swift
//  InterviewJuly28
//
//  Created by John Lollar on 7/28/22.
//

import UIKit

typealias TableDataSource = UITableViewDiffableDataSource<Int, HighSchool>

class ViewController: UIViewController, UITableViewDelegate {
    
    var highSchools = [HighSchool]()
    let tableView = UITableView(frame: .zero, style: .insetGrouped)
    var selectedSchool: HighSchool?
    
    lazy var dataSource: TableDataSource = {
        let dataSource = TableDataSource(tableView: tableView, cellProvider: { (tableView, indexPath, model) -> UITableViewCell in
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.textLabel?.text = model.school_name
            return cell
        })
        
        return dataSource
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchJSON()
        setupUI()
    }
    
    func setupUI() {
        tableView.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: "cell")
        view.addSubview(tableView)
        tableView.frame = view.bounds
        tableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tableView.delegate = self
        
        self.title = "NYC High Schools"
    }
    
    func updateDataSource() {
        var snapshot = dataSource.snapshot()
        snapshot.appendSections([0])
        snapshot.appendItems(highSchools, toSection: 0)
        dataSource.apply(snapshot)
    }
    
    func fetchJSON() {
        let urlString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard let dataResult = data else { return }
            
            do {
                let schools = try JSONDecoder().decode([HighSchool].self, from: dataResult)
                self.highSchools = schools
                
                DispatchQueue.main.async {
                    self.updateDataSource()
                }
                
            } catch {
                print("Error: \(error)")
            }
            
        }.resume()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let highSchool = dataSource.itemIdentifier(for: indexPath) else { return }
        selectedSchool = highSchool
        
        if let detailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailVC") as? DetailViewController {
            detailVC.school = selectedSchool
            
            if let navigator = navigationController {
                navigator.pushViewController(detailVC, animated: true)
            }
        }
    }
}

