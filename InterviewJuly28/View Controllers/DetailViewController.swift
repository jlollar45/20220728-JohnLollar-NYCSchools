//
//  DetailViewController.swift
//  InterviewJuly28
//
//  Created by John Lollar on 7/28/22.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var schoolLabel: UILabel!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var websiteImageView: UIImageView!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var phoneImageView: UIImageView!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var overviewTextView: UITextView!
    @IBOutlet weak var readingLabel: UILabel!
    @IBOutlet weak var mathLabel: UILabel!
    @IBOutlet weak var writingLabel: UILabel!
    
    var school: HighSchool?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let highSchool = school else { return }

        updateUI(school: highSchool)
        fetchScores(dbn: highSchool.dbn)
        // Do any additional setup after loading the view.
    }
    
    func updateUI(school: HighSchool) {
        schoolLabel.numberOfLines = 2
        schoolLabel.adjustsFontSizeToFitWidth = true
        
        schoolLabel.text = school.school_name
        locationLabel.text = school.location
        websiteLabel.text = school.website
        phoneLabel.text = school.phone_number
        
        locationLabel.numberOfLines = 2
        locationLabel.adjustsFontSizeToFitWidth = true
        
        overviewTextView.text = school.overview_paragraph
    }
    
    func updateScores(scores: [SATScores]) {
        readingLabel.text = scores[0].sat_critical_reading_avg_score
        mathLabel.text = scores[0].sat_math_avg_score
        writingLabel.text = scores[0].sat_writing_avg_score
    }

    func fetchScores(dbn: String) {
        
        let urlString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=\(dbn)"
        
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            
            guard let dataResult = data else { return }
            
            do {
                let scores = try JSONDecoder().decode([SATScores].self, from: dataResult)
                
                DispatchQueue.main.async {
                    
                    if scores.count > 0 {
                        self.updateScores(scores: scores)
                    } else {
                        self.readingLabel.text = "N/A"
                        self.mathLabel.text = "N/A"
                        self.writingLabel.text = "N/A"
                    }
                }
            } catch {
                print("Error: \(error)")
            }
        }.resume()
    }
}
