//
//  HighSchool.swift
//  InterviewJuly28
//
//  Created by John Lollar on 7/28/22.
//

import Foundation

struct HighSchool: Codable, Hashable {
    var dbn: String
    var school_name: String
    var overview_paragraph: String
    var location: String
    var website: String
    var phone_number: String
}
