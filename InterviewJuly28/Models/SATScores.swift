//
//  SATScores.swift
//  InterviewJuly28
//
//  Created by John Lollar on 7/28/22.
//

import Foundation

struct SATScores: Codable {
    var sat_critical_reading_avg_score: String
    var sat_math_avg_score: String
    var sat_writing_avg_score: String
}
